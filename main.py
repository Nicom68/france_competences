import pandas as pd
import mysql.connector
import openpyxl
from mysql.connector import Error, IntegrityError




xlsx = pd.ExcelFile("Referentiel-des-NPEC-15.10.2023_vMAJ-29.01.2024.xlsx")
df3 = pd.read_excel(xlsx, "Onglet 3 - référentiel NPEC", skiprows=3)

df3.columns=["code_rncp", "libelle_certif", "certificateur", "diplome", "code_CPNE","nom_CPNE", "npec_final", "statut","date"]

# df3['npec_final'] = df3['npec_final'].replace('NaN', pd.NA)

# # Fill NaN values with 0 and convert to int
# df3['npec_final'] = df3['npec_final'].fillna(0).astype(int)


df3['date'] = df3['date'].apply(lambda x: x.strftime('%Y-%m-%d'))


print(df3['date'].dtype)


# dataframe onglet 4 "convention  collective"
df4 = pd.read_excel(xlsx, "Onglet 4 - CPNE-IDCC", skiprows=2)

df4.columns=["code_CPNE","nom_CPNE","idcc"]


# connexion à la base de données

import sqlite3
con = sqlite3.connect("france_competences.db")

cur = con.cursor()

cur.execute ("""CREATE TABLE IF NOT EXISTS CPNE (code_CPNE integer, nom_CPNE text)""")
cur.execute ("""CREATE TABLE IF NOT EXISTS certification  (code_rncp INT PRIMARY KEY, libelle_certif VARCHAR(255), certificateur VARCHAR(255), diplome VARCHAR(255))""")
cur.execute ("""CREATE TABLE IF NOT EXISTS financement  (code_CPNE INT REFERENCES CPNE(code_CPNE), code_rncp INT REFERENCES certification(code_rncp), statut CHECK (statut IN ('A', 'CPNE', 'R')) NOT NULL DEFAULT 'A', npec_final INT64, date DATE)""")
cur.execute ("""CREATE TABLE IF NOT EXISTS convention_collective (idcc INT PRIMARY KEY, code_CPNE INT REFERENCES CPNE(code_CPNE))""")

# table "CPNE", insertion des données
for index, row in df3.iterrows():
    if pd.notna(row['code_CPNE']) and pd.notna(row['nom_CPNE']):
        cur.execute("INSERT INTO CPNE(code_CPNE, nom_CPNE) VALUES (?, ?)", (row['code_CPNE'], row['nom_CPNE']))
        code_CPNE = cur.lastrowid

# table "certification", insertion des données
for index, row in df3.iterrows():
    try:
        cur.execute("INSERT INTO certification(code_rncp, libelle_certif, certificateur, diplome) VALUES (?, ?, ?, ?)", (row['code_rncp'], row['libelle_certif'], row["certificateur"], row["diplome"]))
        code_CPNE = cur.lastrowid
    except:
        continue


# table "financement", insertion des données
for index, row in df3.iterrows():
        cur.execute("INSERT INTO financement(code_CPNE, code_rncp, statut, npec_final,date) VALUES (?,?,?,?,?)", (row ['code_CPNE'],row["code_rncp"], row['statut'],row['npec_final'], row["date"]))
        code_CPNE = cur.lastrowid
    
# # table "financement", insertion des données
# for index, row in df3.iterrows():
#         cur.execute("INSERT INTO financement(code_CPNE, code_rncp,statut, npec_final) VALUES (?,?,?,?)", (row ['code_CPNE'],row["code_rncp"], row['statut'], row['npec_final']))
#         code_CPNE = cur.lastrowid
    
    
   
# table "convention collective", insertion des données
for index, row in df4.iterrows():
    if pd.notna(row['idcc']) and pd.notna(row['code_CPNE']):
        cur.execute("INSERT INTO convention_collective (idcc, code_CPNE) VALUES (?, ?)", (row['idcc'], row['code_CPNE']))
        idcc = cur.lastrowid

    
    con.commit() 









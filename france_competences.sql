DROP DATABASE IF EXISTS france_competences;
CREATE DATABASE france_competences;
USE france_competences;




CREATE TABLE CPNE (
  code_CPNE integer,
  nom_CPNE text
);

CREATE TABLE convention_collective (
  idcc INT PRIMARY KEY,
  code_CPNE INT REFERENCES CPNE(code)
);

CREATE TABLE financement (
  id INTEGER PRIMARY KEY,
  statut ENUM("A","CPNE", "R"),
  npec_final INT,
  date date,
  code_cpne INT REFERENCES CPNE(code),
  code_rncp INT REFERENCES certification(code_rncp)
);

CREATE TABLE certification (
  code_rncp INT PRIMARY KEY,
  libelle_certif VARCHAR(255),
  certificateur VARCHAR(255),
  diplome VARCHAR(255)
);





